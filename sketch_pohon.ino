// U8glib - Version: Latest
#include <U8glib.h>

//0.5ns timing library
#include <eRCaGuy_Timer2_Counter.h>

// Display
#include <LiquidCrystal.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define MICROS() timer2.get_count()

struct StopWatch {
    StopWatch() {
      Start();
    }

    void Start() {
      Now = MICROS();
      StartTime = Now;
    }

    float Toc() {
      return (float)TocUL();
    }

    bool DidTimeElapsedFromLastCall(float dt) {
      return TocUL() > dt * 2;
    }

    bool DidTimeElapsedFromLastCallWithReset(float dt) {
      float t = TocUL();
      if (t > dt * 2) {
        StartTime = Now;
        return true;
      }
      return false;
    }

    void WaitUntilAndRestart(float dt) {
      unsigned long dt2 = round(dt * 2.0);
      unsigned long t2 = StartTime + dt2;
      if (t2 > StartTime) {
        unsigned long t = MICROS();
        while (t < t2) {
          t = MICROS();
        }
        StartTime = t;
      } else {
        // t2 overflow
        unsigned long t = MICROS();
        while (t > StartTime) {
          t = MICROS();
        }
        while (t < t2) {
          t = MICROS();
        }
        StartTime = t;
      }
    }

    bool DidTimeElapsedInMicroSec(unsigned long dt) {
      return TocUL() > dt * 2;
    }

    bool DidTimeElapsedInSec(float dt) {
      return TocUL() / 1000000 > dt * 2;
    }

  private:
    unsigned long TocUL() {
      Now = timer2.get_count();
      if (StartTime > Now) {
        return (((unsigned long) - 1) - StartTime + Now) / 2;
      } else {
        return (Now - StartTime) / 2;
      }
    }

    unsigned long Now;
    unsigned long StartTime;
};

StopWatch DebugWatches;


struct StepperMotorConfiguration {
    StepperMotorConfiguration(uint8_t enaPin, uint8_t dirPin, uint8_t pulPin, bool enaHigh, bool dirHigh, bool pulHigh, long stepsPerRev)
      : Ena(enaPin)
      , Dir(dirPin)
      , Pul(pulPin)
      , StepsPerRev(stepsPerRev) {
      EnaHigh = enaHigh ? HIGH : LOW;
      EnaLow = !enaHigh ? HIGH : LOW;
      DirHigh = dirHigh ? HIGH : LOW;
      DirLow = !dirHigh ? HIGH : LOW;
      PulHigh = pulHigh ? HIGH : LOW;
      PulLow = !pulHigh ? HIGH : LOW;
      PulVal = false;
      pinMode(Ena, OUTPUT);
      pinMode(Dir, OUTPUT);
      pinMode(Pul, OUTPUT);
    }

    inline void SetEnaHigh() {
      digitalWrite(Ena, EnaHigh);
    }

    inline void SetEnaLow() {
      digitalWrite(Ena, EnaLow);
    }

    inline void SetDirHigh() {
      digitalWrite(Dir, DirHigh);
    }

    inline void SetDirLow() {
      digitalWrite(Dir, DirLow);
    }

    inline void SetPulHigh() {
      digitalWrite(Pul, PulHigh);
    }

    inline void SetPulLow() {
      digitalWrite(Pul, PulLow);
    }

    inline void ChangePulValue() {
      PulVal = !PulVal;
      digitalWrite(Pul, PulVal);
    }

    inline long GetStepsPerRev() {
      return StepsPerRev;
    }

    const long StepsPerRev;

  public:
    uint8_t Ena;
    uint8_t Dir;
    uint8_t Pul;
    int EnaHigh;
    int DirHigh;
    int PulHigh;
    int EnaLow;
    int DirLow;
    int PulLow;
    bool PulVal;
};

struct StepperMotor {
    StepperMotor(StepperMotorConfiguration &configuration) : Config(configuration) {
      Config.SetEnaLow();
    }

    bool TurnInit() {
      Config.SetEnaHigh();
      pulsePerRevSpeedConstantDuringAcc = (1000000.0 / Config.StepsPerRev) * 1e6;
      pulsePerRevSpeedConstantDuringCstSpeed = 1000000.0 / Config.StepsPerRev;
      pulseAtDesiredVel = pulsePerRevSpeedConstantDuringCstSpeed / (vel);
      // MINIMAL REVOLUTIONS INTRODUCED! Otherwise first pulse revolution is too high.
      // This is caused because getPulseTimeInMicroSecWhenAcc with really small first argument (absolute time) computes very long pulse 
      pulseTimeForMinVelocity = 1000000.0 / (0.1 * Config.StepsPerRev);
      turnWatches.Start();
      stepWatches.Start();
      if (vel <= 0.001)
        return false;
      return true;
    }

    float computedVel, pulse;

    void Turn() {
      Step(getPulseTimeInMicroSecWhenAcc(turnWatches.Toc()));
    }

    void EnsureSwitchedOff() {
      Config.SetEnaLow();
    }

    void TurnPositiveDirection() {
      Config.SetDirHigh();
    }

    void TurnNegativeDirection() {
      Config.SetDirLow();
    }

    void SetSpeed(float speed) {
      vel = fabs(speed / PI);
    }

    void SetAcceleration(float acceleration) {
      acc = fabs(acceleration * PI);
    }

    void StopTurn() {
      Config.SetEnaLow();
    }
    
  private:
    float getPulseTimeInMicroSecWhenAcc(float timeInMicroSec) {
      auto pulse = pulsePerRevSpeedConstantDuringAcc / (acc * timeInMicroSec);
      return pulse > pulseAtDesiredVel ? pulse : pulseAtDesiredVel;
    }

    void Step(float pulseInMicroSec) {
      pulseInMicroSec = min(pulseInMicroSec, pulseTimeForMinVelocity);
      stepWatches.WaitUntilAndRestart(pulseInMicroSec);
      Config.ChangePulValue();
      delayMicroseconds(4);
      Config.ChangePulValue();
    }

    float acc;
    float vel;
    float pulsePerRevSpeedConstantDuringAcc;
    float pulsePerRevSpeedConstantDuringCstSpeed;
    float pulseTimeForMinVelocity;
    float pulseAtDesiredVel;
    StopWatch turnWatches;
    StopWatch stepWatches;
    StepperMotorConfiguration &Config;
};

struct DynamicsTable {
  int acc;
  int vel;
};


struct Display {
    Display(LiquidCrystal_I2C& disp) : lcd(disp) {
    }

    void init() {
      lcd.init();
    }

    void redraw(DynamicsTable *table) {
      drawAcceleration(String(table->acc));
      drawVelocity(String(table->vel));
    }

private:

    void drawAcceleration(String acceleration) {
      lcd.backlight();
      lcd.setCursor(0,0);
      lcd.print("zrychleni:");
      lcd.setCursor(11,0);
      lcd.print("   ");
      lcd.setCursor(11,0);
      lcd.print(acceleration);
    }

    void drawVelocity(String velocity) {
      lcd.backlight();
      lcd.setCursor(0,1);
      lcd.print("otacky:");
      lcd.setCursor(11,1);
      lcd.print("   ");
      lcd.setCursor(11,1);
      lcd.print(velocity);
    }

    LiquidCrystal_I2C &lcd;
};

struct PullUpButton {
  PullUpButton(uint8_t readPin) : ReadPin(readPin) {
    pinMode(ReadPin, INPUT_PULLUP);
  }

  bool isOn() {
    return digitalRead(ReadPin) == HIGH;
  }

  uint8_t ReadPin;
};

struct Keyboard3 {
  Keyboard3(int up, int down, int dot, int debounce_delay_) {
    pin_id[0] = up;
    pin_id[1] = down;
    pin_id[2] = dot;
    for (int i = 0; i < 3; i++) {
      pin_acknowledged[i] = true;
      pin_pressed[i] = false;
      pinMode(pin_id[i], INPUT_PULLUP);
    }
    debounce_delay = debounce_delay_;
  }
  
  void cyclic() {
    check_button_pressed(0);
    check_button_pressed(1);
    check_button_pressed(2);
  }
  
  bool is_up_pressed() {
    return try_acknowledge(0);
  }
  
  bool is_down_pressed() {
    return try_acknowledge(1);
  }
  
  bool is_dot_pressed() {
    return try_acknowledge(2);
  }

private:
  void check_button_pressed(int id) {
    if (digitalRead(pin_id[id]) == LOW) {
      if (!pin_pressed[id]) {
        pin_pressed[id] = true;
        pin_acknowledged[id] = false;
        delay(debounce_delay);
      }
    } else {
      if (pin_acknowledged[id] == true) {
        pin_pressed[id] = false;
        delay(debounce_delay);
      }
    }
  }

  bool try_acknowledge(int id) {
    if (!pin_acknowledged[id]) {
      pin_acknowledged[id] = true;
      return true;
    }
    return false;
  }
    
  int pin_id[3];
  int pin_acknowledged[3];
  int pin_pressed[3];
  int debounce_delay;
};

struct MotorSystem {
  MotorSystem(Display& display_, PullUpButton &button_, Keyboard3 &acc_ctrl_, Keyboard3 &vel_ctrl_, StepperMotor &motor_)
    : display(display_)
    , button(button_)
    , acc_ctrl(acc_ctrl_)
    , vel_ctrl(vel_ctrl_)
    , motor(motor_)
  {
  }

  void Init() {
    state.vel = 200;
    state.acc = 60;
    motor.SetSpeed(state.vel);
    motor.SetAcceleration(state.acc);
    display.init();
    display.redraw(&state);
  }

  void Cyclic() {
    if (button.isOn()) {
      if (!motor_started) {
        delay(100);
        motor_started = true;
        motor.TurnInit();
      }
      motor.Turn();
    } else {
      motor.StopTurn();
      acc_ctrl.cyclic();
      vel_ctrl.cyclic();
      
      if (acc_ctrl.is_up_pressed()) {
        state.acc = min(600, state.acc + 5);
        motor.SetAcceleration(state.acc);
        display.redraw(&state);
      }
      if (acc_ctrl.is_down_pressed()) {
        state.acc = max(0, state.acc - 5);
        motor.SetAcceleration(state.acc);
        display.redraw(&state);
      }
      if (vel_ctrl.is_up_pressed()) {
        state.vel = min(600, state.vel + 10);
        motor.SetSpeed(state.vel);
        display.redraw(&state);
      }
      if (vel_ctrl.is_down_pressed()) {
        state.vel = max(0, state.vel - 10);
        motor.SetSpeed(state.vel);
        display.redraw(&state);
      }
      
      if (vel_ctrl.is_dot_pressed()) {
        motor.TurnPositiveDirection();
      }
      if (acc_ctrl.is_dot_pressed()) {
        motor.TurnNegativeDirection();
      }
      motor_started = false;
    }
  }

  DynamicsTable state;
  Display &display;
  PullUpButton &button;
  Keyboard3 &acc_ctrl;
  Keyboard3 &vel_ctrl;
  StepperMotor &motor;
  bool motor_started;
  bool initialized;
};

//////////////////////////////////////////
// Deployment
//////////////////////////////////////////

LiquidCrystal_I2C LCD(0x27, 20, 4);
Display ControlDisplay(LCD);
PullUpButton SwitchButton(11);
Keyboard3 AccControl(3, 2, 4, 10);
Keyboard3 VelControl(6, 5, 7, 10);
StepperMotorConfiguration MotorConfig(8, 9, 10, false, true, true, 400);
StepperMotor Nema34_13Nm(MotorConfig);
MotorSystem ControlPanel(ControlDisplay, SwitchButton, AccControl, VelControl, Nema34_13Nm);

//bool switchIsOn = false;
unsigned long nrLoops = 0;

void setup() {
  //Serial.begin(9600);
  ControlPanel.Init();
}

void loop() {
  ControlPanel.Cyclic();
}
